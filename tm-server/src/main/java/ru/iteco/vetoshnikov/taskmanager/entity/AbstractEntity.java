package ru.iteco.vetoshnikov.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity {
    @NotNull
    @Id
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "createDate")
    private Date createDate = new Date();
}
