package ru.iteco.vetoshnikov.taskmanager.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.endpoint.*;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.service.*;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;

import javax.xml.ws.Endpoint;

@Component
public class Bootstrap {
    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;
    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;
    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;
    @NotNull
    @Autowired
    private IUserService userService;

    public void init() {
        try {
            setEndpoint();
//            addUserAndAdminUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEndpoint() {
        Endpoint.publish("http://localhost:8080/DomainWebService?wsdl", domainEndpoint);
        Endpoint.publish("http://localhost:8080/ProjectWebService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/TaskWebService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/UserWebService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/SessionWebService?wsdl", sessionEndpoint);
    }

//    private void addUserAndAdminUser() {
//        User guest = new User();
//        guest.setLogin("guest");
//        guest.setPassword(HashUtil.getHash("guest"));
//        guest.setRole(RoleType.USER.getDisplayName());
//        guest.setName("guest");
//        userService.createUser(guest);
//        User admin = new User();
//        admin.setLogin("admin");
//        admin.setPassword(HashUtil.getHash("admin"));
//        admin.setRole(RoleType.ADMINISTRATOR.getDisplayName());
//        admin.setName("admin");
//        userService.createUser(admin);
//    }
}
