package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void mergeTask(
            @WebParam(name = "taskObject") @Nullable TaskDTO taskObject
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );

    @WebMethod
    void clearTask(
    );

    @WebMethod
    TaskDTO findOneTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );

    @WebMethod
    List<TaskDTO> findAllTask(
    );

    @WebMethod
    List<TaskDTO> findAllByProjectTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void createTaskTask(
            @WebParam(name = "taskObject") @Nullable TaskDTO taskObject
    );

    @WebMethod
    void removeAllByProjectTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void loadTask(
            @WebParam(name = "domainObject") @Nullable DomainDTO domainObject
    );

    @WebMethod
    String getIdTaskTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );
}
