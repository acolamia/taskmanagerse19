package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    void mergeProject(
            @WebParam(name = "projectObject") @Nullable ProjectDTO projectObject
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );

    @WebMethod
    void clearProject();

    @WebMethod
    ProjectDTO findOneProject(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );

    @WebMethod
    List<ProjectDTO> findAllProject();

    @WebMethod
    List<ProjectDTO> findAllByUserProject(
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void createProjectProject(
            @WebParam(name = "projectObject") @Nullable ProjectDTO projectObject
    );

    @WebMethod
    void removeAllByUserProject(
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void loadProject(
            @WebParam(name = "domainObject") @Nullable DomainDTO domainObject
    );

    @WebMethod
    String getIdProjectProject(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );
}