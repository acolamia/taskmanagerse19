package ru.iteco.vetoshnikov.taskmanager.util;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.dto.SessionDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

public class SignatureUtil {
    private static final String salt = "vetoshnikov";
    private static final Integer cycle = 111;

    @Nullable
    public static String sign(@Nullable final Session session) {
        String json = session.getId() + session.getUser().getId() + session.getTimestamp();
        return sign(json);
    }

    @Nullable
    public static String sign(@Nullable final SessionDTO session) {
        String json = session.getId() + session.getUserId() + session.getTimestamp();
        return sign(json);
    }

    @Nullable
    public static String sign(@Nullable final String value) {
        if (value == null) {
            return null;
        }
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.getHash(salt + result + salt);
        }
        return result;
    }

    public static SessionDTO check(@Nullable final SessionDTO session) {
        if (session == null) return null;
        String signature = session.getSignature();
        session.setSignature(null);
        session.setSignature(sign(session));
        if (!session.getSignature().equals(signature)) return null;
        return session;
    }
}
