package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.dto.DomainDTO;
import ru.iteco.vetoshnikov.taskmanager.dto.ProjectDTO;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.service.ProjectService;
import ru.iteco.vetoshnikov.taskmanager.util.ConvertEndtityAndDTOUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Component
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ConvertEndtityAndDTOUtil convertUtil;

    @Override
    @WebMethod
    public void createProjectProject(
            @WebParam(name = "projectObject") @Nullable final ProjectDTO projectObject
    ) {
        Project project = convertUtil.convertDTOToProject(projectObject);
        projectService.createProject(project);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "projectObject") @Nullable final ProjectDTO projectObject
    ) {
        Project project = convertUtil.convertDTOToProject(projectObject);
        projectService.createProject(project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) {
        projectService.remove(userId, projectName);
    }

    @Override
    @WebMethod
    public void clearProject(
    ) {
        projectService.clear();
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) {
        return convertUtil.convertProjectToDTO(projectService.findOne(userId, projectName));
    }

    @Override
    @WebMethod
    public List<ProjectDTO> findAllProject(
    ) {
        return convertUtil.convertProjectToDTOList(projectService.findAll());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> findAllByUserProject(
            @WebParam(name = "userId") @Nullable final String userId
    ) {
        return convertUtil.convertProjectToDTOList(projectService.findAllByUser(userId));
    }

    @Override
    @WebMethod
    public void removeAllByUserProject(
            @WebParam(name = "userId") @Nullable final String userId
    ) {
        projectService.removeAllByUser(userId);
    }

    @Override
    @WebMethod
    public void loadProject(
            @WebParam(name = "domainObject") @Nullable final DomainDTO domainObject
    ) {
        projectService.load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdProjectProject(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "projectName") @Nullable final String projectName
    ) {
        return projectService.getIdProject(userId, projectName);
    }

}