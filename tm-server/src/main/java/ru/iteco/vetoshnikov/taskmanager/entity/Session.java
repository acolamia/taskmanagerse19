package ru.iteco.vetoshnikov.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class Session{
    @NotNull
    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Nullable
    @Column(name = "timestamp")
    private Long timestamp = new Date().getTime();
    @Nullable
    @Column(name = "signature")
    private String signature;
}
