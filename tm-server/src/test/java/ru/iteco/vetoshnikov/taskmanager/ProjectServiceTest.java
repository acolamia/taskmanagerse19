package ru.iteco.vetoshnikov.taskmanager;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfigTest.class)
public class ProjectServiceTest extends Assert {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    private User user;

    private Project testProject;
    private Project testProject2;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setLogin("test");
        user.setPassword("test");
        user.setRole(RoleType.USER.getDisplayName());
        userService.createUser(user);

        testProject = new Project();
        testProject.setName("testProject");
        testProject.setUser(user);
        testProject2 = new Project();
        testProject2.setName("testProject2");
        testProject2.setUser(user);
        projectService.createProject(testProject);
        projectService.createProject(testProject2);
    }

    @After
    public void tearDown() throws Exception {
        projectService.removeAllByUser(user.getId());
        userService.remove(user.getId());
    }

    @Test
    public void createProject() {
        Project getProject = projectService.findOne(user.getId(), testProject.getName());
        assertNotNull(testProject);
        assertNotNull(getProject);
        assertEquals(testProject.getName(), getProject.getName());
    }

    @Test
    public void merge() {
        testProject.setName("testProjectRename");
        testProject2.setName("testProject2Rename");
        List<Project> projectList = new ArrayList<>();
        projectList.add(testProject);
        projectList.add(testProject2);
        projectService.merge(projectList);
        List<Project> getProjectList = projectService.findAllByUser(user.getId());
        for (Project project : getProjectList) {
            assertNotNull(project);
            if (project.getId().equals(testProject.getId())) assertEquals(project.getName(), testProject.getName());
            if (project.getId().equals(testProject2.getId())) assertEquals(project.getName(), testProject2.getName());
        }
    }

    @Test
    public void remove() {
        projectService.remove(user.getId(), testProject.getName());
        Project project = projectService.findOne(user.getId(), testProject.getName());
        assertNull(project);
    }

    @Test
    public void removeAllByUser() {
        projectService.removeAllByUser(user.getId());
        List<Project> projectList = projectService.findAllByUser(user.getId());
        for (Project project : projectList) {
            assertNull(project);
        }
    }

    @Test
    public void clear() {
        projectService.clear();
        List<Project> projectList = projectService.findAll();
        for (Project project : projectList) {
            assertNull(project);
        }
    }

    @Test
    public void findOne() {
        Project project = projectService.findOne(user.getId(), testProject.getName());
        assertNotNull(project);
        assertEquals(project.getName(), testProject.getName());
    }

    @Test
    public void findAll() {
        List<Project> projectList = null;
        projectList = projectService.findAll();
        assertFalse(projectList.isEmpty());
    }

    @Test
    public void findAllByUser() {
        List<Project> projectList = null;
        projectList = projectService.findAllByUser(user.getId());
        assertFalse(projectList.isEmpty());
    }

    @Test
    public void getIdProject() {
        Project project = projectService.findOne(user.getId(), testProject.getName());
        assertNotNull(project);
        assertEquals(project.getId(), testProject.getId());
    }
}