package ru.iteco.vetoshnikov.taskmanager.command.help;

import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public final class HelpCommand extends AbstractCommand {
    public HelpCommand() {
        super();
        setSecure(false);
    }

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Список команд.";
    }

    @Override
    public void execute() {
        System.out.println("Список команд:");
        @NotNull final Map<String, AbstractCommand>map=serviceLocator.getCommandService().getCommandMap();
        for (@NotNull  Map.Entry<String, AbstractCommand> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue().description());
        }
    }
}
