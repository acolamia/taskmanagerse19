package ru.iteco.vetoshnikov.taskmanager.api.customApi;

import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IProjectEndpoint;

import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceFeature;

public interface IProjectEndpointService {
    @WebEndpoint(name = "ProjectEndpointPort")
    IProjectEndpoint getProjectEndpointPort();

    @WebEndpoint(name = "ProjectEndpointPort")
    IProjectEndpoint getProjectEndpointPort(WebServiceFeature... features);
}
